#!/bin/bash
set -e

chown -R redis .

cmd="redis-sentinel /etc/redis-sentinel.conf"
cmd="$cmd --'sentinel monitor mymaster $REDIS_PORT_6379_TCP_ADDR $REDIS_PORT_6379_TCP_PORT 2'"
cmd="$cmd --'sentinel down-after-milliseconds mymaster 60000'"
cmd="$cmd --'sentinel failover-timeout mymaster 180000'"
cmd="$cmd --'sentinel parallel-syncs mymaster 1'"

eval gosu redis $cmd