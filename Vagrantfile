# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

mybox = ENV['BOX'] || 'ubuntu/precise64'


Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.define "jenkins-master" do |node_config|
    node_config.vm.box = "#{mybox}"
    node_config.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.memory = 2048
      vb.cpus = 1
    end

    node_config.vm.hostname = "jenkins-master.vagrant.intern"
    node_config.vm.network :private_network, ip: "192.168.1.40"
    node_config.vm.network :forwarded_port, guest: 8080, host: 8080
  end

  (1..2).each do |instance|
    groupoffset = 40
    hostid = "0#{instance}"

    config.vm.define "jenkins-slave-#{hostid}" do |node_config|

      node_config.vm.box = "#{mybox}"
      node_config.vm.provider "virtualbox" do |vb|
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.memory = 2048
        vb.cpus = 1
      end

      node_config.vm.hostname = "jenkins-slave-#{hostid}.vagrant.intern"
      node_config.vm.network :private_network, ip: "192.168.1.#{instance + groupoffset}"
    end
  end

  # puppet provisioning
  config.vm.provision :shell, inline: $update_puppet_script
  config.vm.provision 'puppet' do |puppet|
    puppet.manifests_path = 'puppet/manifests'
    puppet.manifest_file = 'default.pp'
    puppet.module_path = 'puppet/modules'
    puppet.hiera_config_path = 'puppet/hiera.yaml'
    puppet.working_directory = '/etc/puppet'
    puppet.options = '--verbose --parser=future'
    puppet.facter = {
      'vagrant' => true
    }
  end
end

$update_puppet_script = <<SCRIPT
if [[ `puppet -V` != '3.8.3' ]]; then
  wget -q https://apt.puppetlabs.com/puppetlabs-release-precise.deb
  dpkg -i puppetlabs-release-precise.deb
  apt-get -q update
  apt-get install -q -y puppet
  gem install deep_merge
  gem install hiera-file
fi
SCRIPT
