export DOCKER_HOST=tcp://127.0.0.1:4243;

cat << EOF > $WORKSPACE/docker-compose.yml
redis:
  container_name: redis
  image: redis
  ports:
    - "6379"

redis-sentinel:
  container_name: redis-sentinel
  image: matthiaswiesner/redis-sentinel
  ports:
    - "26379"
  links:
   - redis:redis

webservice:
  container_name: webservice
  command: /bin/bash /src/make_test.sh
  image: matthiaswiesner/geo_webservice
  links:
   - redis-sentinel:redis-sentinel
  ports:
   - "5000:5000"
  volumes:
   - $WORKSPACE:/src
   - $WORKSPACE/data:/data
  environment:
   - C_INCLUDE_PATH=/usr/include/gdal
   - CPLUS_INCLUDE_PATH=/usr/include/gdal
   - DATA_DIR=/data
   - VENV_DIR=/home/.venv
   - CONFIG_DIR=/src/image_download_service/config
  working_dir: /src
EOF

docker-compose -f $WORKSPACE/docker-compose.yml run --rm webservice
docker-compose stop
docker-compose rm -fv
