cat << EOF1 > $WORKSPACE/make_test.sh
#!/bin/bash

python << EOF2
import yaml

config = dict(
    redis = dict(
        password = "",
        sentinel = dict(
            name = "mymaster",
            port = "\$REDIS_SENTINEL_PORT_26379_TCP_PORT",
            hosts = [
                "\$REDIS_SENTINEL_PORT_26379_TCP_ADDR"
                ]
            )
    )
)
with open("\$CONFIG_DIR/testing.redis.yaml", "w") as f:
    f.write(yaml.dump(config, default_flow_style=False))
EOF2;

make test;
EOF1

chmod +x $WORKSPACE/make_test.sh;
