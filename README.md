# My Jenkins
This repository is intended to be a *locally* running [jenkins] server. The jenkins server is installed in a vagrant box.
The purpose of installing and running a local jenkins server is to build a machine which executes tests and creates builds for developer projects.

## SSH-Keys
During provisioning, the jenkins default user is assigned a ssh key-pair. Currently this key pair can be used to fetch the image-preview-service repository on bitbucket. (The keys are assigned to jenkins global credentials scope, named *pull_key*.)
You probably want to assign your own key. You have to copy the key pair to the directory: `<myjenkins>/puppet/hieradata/role/jenkins/master.d/`.
But it is not necessary to configure the credentials for your project at this stage. You can define the required credentials during the project configuration later.

## Install
The jenkins server is installed on a vagrant machine and provisioned with puppet. Per default 3 instances are spawned: one master and two build slaves. (Whereby the 2 slaves are not required, so that I continue with only the `jenkins-master`.)

The installation process is rather simple:

1. Install puppet modules
`cd <myjenkins>/puppet; librarian-puppet install;`

    F.Y.I: Currently, some *non-blackbrigde-hosted* puppet repositories are used for the provisioning process. (See: `<myjenkins>/puppet/Puppetfile`). It remains to be decided whether this can be used in general, or whether they need to be cloned to *blackbrigde-hosted* puppet repositories.

2. Install jenkins in vagrant box
`cd <myjenkins>/; vagrant up jenkins-master;`

## Jenkins Configuration
The `jenkins-master`s  IP address is statically configured to `192.168.1.40`.
Since the jenkins server is bound to port `8080`, you can access the jenkins server with the url http://192.168.1.40:8080.

There is not much jenkins server configuration effort. 
But, a good moment to configure (add/remove) the credentials that are needed for the developers projects. The credentials can be configured at http://192.168.1.40:8080/credential-store. The credentials are ordered by scopes. The *Global credentials* scope is preconfigured as default. That is the scope where the *pull_key* is assigned to. It's possible to create new scopes and credentials with ssh-key, certificate or username/password.

## Configure job
It's theoretical possible to create and configure the jobs on provisioning process. The jobs specs are stored in xml files (/var/lib/jenkins/jobs/<job_name>/config.xml). And if it's needed to automate this step, such a xml file can be handled by puppet provisioning. But this process is rather confusing, so I describe the manually process.

Here I describe the configuration steps based on running image_download_service unit and behaviour tests. Thats why I'm using here project relevant data.

Create a new job - *New Item* and choose *Freestyle project*. 

Choose *Git* from *Source Code Management* and add the bitbucket URL `git@bitbucket.org:bbpd/image-preview-service.git` to *Repository URL*. The jenkins git plugin checks immediately the accessability, so choose the proper credentials, here the *pull_key*. The *public key from the key pair has to be a assigned to the projects repository on bitbucket*. Hopefully you have the admin permissions to your repository, otherwise you have to ask the IT department :-/  
Next, specify the projects branch in field *Branch Specifier*. In this case, the developer branch is to be tested, so fill in with `*/develop`.

The *Build Triggers* are still currently unimportant. In a CI process, you probably have to check `Build when a change is pushed to BitBucket`, but this behaviour is still untested.

### Build
The build steps are the more demanding and project-specific configuration steps that you need to prepare for your project requirements.

It should be clarified, what the services requirements are. In this case, there are:

1. The service needs specific testdata
2. The service needs a redis-sentinel access to cache data
3. The service runs on a preconfigured linux base image with:
    3.1. GDAL 1.10
    3.2. a python virtual environment 

#### Get the testdata
The vagrant machine is configured to mount `ds04-2.devel.intern:/vol/DEVEL_PLATFORMDEV_TESTDATA` automatically during the provisioning to `/mnt/nfs` by default. 
The image download service expects its testdata stored on this nfs share in `<mountpoint>/testdata/image_download_service`. All what needs to be configured, is to add this nfs directory to the dockers shared folder list (called "volumes", see 2nd buildstep).

#### Build step to prepare testrun
First of all, the tests and builds are running in an isolated environment, they have to, to ensure that the jenkins server vagrant box will not be contaminated.  
The isolated environment is guaranteed by using docker container for the jenkins builds.

##### Runfile
The image download service needs a redis server which is accessed via a redis sentinel. For both are spawned a separate docker container too. Typically, the docker containers are separated from each other. Therefore it is a bit tricky to transmit the corresponding credentials from one docker container to another. The way to get to do this is by linking the dockers container one to another. In this way, some general, but also custom environment variables are exposed from the source container to the target container .

The image download service reads the redis-sentinel credentials from a project-specific config file (`/image_download_service/config/testing.redis.yaml`). In this build step a file has to be created that reads the redis-sentinel credentials from the environment variables and writes that redis config file.

Add a *Execute shell* build step and fill it with:
~~~bash
cat << EOF1 > $WORKSPACE/make_test.sh
#!/bin/bash

python <(cat << EOF2
import yaml

config = dict(
    redis = dict(
        password = "",
        sentinel = dict(
            name = "mymaster",
            port = "\$REDIS_SENTINEL_PORT_26379_TCP_PORT",
            hosts = [
                "\$REDIS_SENTINEL_PORT_26379_TCP_ADDR"
                ]
            )
    )
)
with open("\$CONFIG_DIR/testing.redis.yaml", "w") as f:
    f.write(yaml.dump(config, default_flow_style=False))
EOF2);

make test;
EOF1

chmod +x $WORKSPACE/make_test.sh;
~~~
(To use the pythohn yaml module it have to be installed in the docker image, outside of the virtual environment.)

As you can see, jenkins creates in this build step a file `$WORKSPACE/make_test.sh`. This file will be executed in the next build step from within the container (the complete jenkins $WORKSPACE is mounted to `/src` within the docker container). The `make_test.sh` file creates first the redis configuration and runs `make test`.

#### Build step to run the tests
The last step puts all in together, spawns the nodes and runs the test. We're using `docker-compose`, a wrapper tool which simplifies the spawning, linking and stopping docker commands.

Add a *Execute shell* build step and fill it with:
~~~bash
export DOCKER_HOST=tcp://127.0.0.1:4243;

cat << EOF > $WORKSPACE/docker-compose.yml
redis:
  container_name: redis
  image: redis
  ports:
    - "6379"

redis-sentinel:
  container_name: redis-sentinel
  image: matthiaswiesner/redis-sentinel
  ports:
    - "26379"
  links:
   - redis:redis

webservice:
  container_name: webservice
  command: /bin/bash /src/make_test.sh
  image: matthiaswiesner/geo_webservice
  links:
   - redis-sentinel:redis-sentinel
  ports:
   - "5000:5000"
  volumes:
   - $WORKSPACE:/src
   - /mnt/nfs/testdata/image_download_service:/data
  environment:
   - C_INCLUDE_PATH=/usr/include/gdal
   - CPLUS_INCLUDE_PATH=/usr/include/gdal
   - DATA_DIR=/data
   - VENV_DIR=/home/.venv
   - CONFIG_DIR=/src/image_download_service/config
  working_dir: /src
EOF

docker-compose -f $WORKSPACE/docker-compose.yml run --rm webservice
docker-compose stop
docker-compose rm -fv
~~~
Attention! You have to set the environment variable `DOCKER_HOST` to `tcp://127.0.0.1:4243`. To get jenkins executes docker commands, it's needed to to run docker over a tcp socket. Docker is configured accordingly during the provisioning.

Unfortunately, docker-compose needs to run a `docker-compose.yml` file, so that this build step have to create this yaml file first. The file's structure is rather trivial. Here the docker containers parameter are described. The webservice's environment variables are rather service specific:
   * C_INCLUDE_PATH, CPLUS_INCLUDE_PATH are needed to compile the python gdal binding
   * DATA_DIR, VENV_DIR, CONFIG_DIR are needed to tell the service some file locations. This became necessary as the vagrant box uses different file structures than the docker container.

The "volumes" configuration defines the shared folders between host and docker.
During the docker-run, the jenkins workspace is shared with the container. This approach is much faster than the *copy-to-container* approach and an eventually build result is accessable to the jenkins server!
As mentioned before, also the mounted testdata directory is shared with the docker.

### Images
A docker container is an instance of a docker image (similar to objects and classes). An image is made of file system layers (AUFS). If the service needs a special setup, a good approach is to create a special image for this. For the image download service I created 2 special images.

Attention! Currently, both images are public and hosted on my DockerHub account. I will let them there as they are, but it would be a good idea to have a separate DockerHub account for Blackbridge.

#### Redis-Sentinel Image
The redis sentinel image is based on the redis image, which already defines an entrypoint (this is important since the entrypoint is always executed). During creating the image another `entrypoint.sh` script overwrites the redis images' script. The entrypoint script executes `redis-sentinel` with the redis server configuration as parameter. This has to be done this way, because 
the redis server IP address and port is not known until after the containers start (and the containers linking).

See the Dockerfile: [redis-sentinel/Dockerfile](docker/redis-sentinel/Dockerfile)  
The image is hosted on DockerHub: [matthiaswiesner/redis-sentinel]

#### Webservice Image
The webservice image is based on the ubuntu:precise image. In a RUN step some additional packages are installed: GDAL 1.10 and some necessary packages to install and setup a python application. In a second step a python virtual environment is built and python-pip and -setuptools are updated (setuptools has a version change from 0.6 to 18.7) within the virtual environment.

See the Dockerfile: [webservice/Dockerfile](docker/webservice/Dockerfile)  
The image is hosted on DockerHub: [matthiaswiesner/geo_webservice]

[jenkins]: https://jenkins-ci.org/
[matthiaswiesner/redis-sentinel]: https://hub.docker.com/r/matthiaswiesner/redis-sentinel/
[matthiaswiesner/geo_webservice]: https://hub.docker.com/r/matthiaswiesner/geo_webservice/
