# The filebucket option allows for file backups to the server
filebucket { main: server => 'puppet.srv.intern' }

# Set global defaults - including backing up all files to the main filebucket and adds a global path
File { backup => main }
Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin" }

# Ged rid of this annoying "Warning: The package type's allow_virtual parameter..."
Package { allow_virtual => true }

$subrole_lvl1 = hiera('subrole_lvl1', '')
$subrole_lvl2 = hiera('subrole_lvl2', '')
$subrole_lvl3 = hiera('subrole_lvl3', '')
$subrole_lvl4 = hiera('subrole_lvl4', '')

if $subrole_lvl4 != '' {
  $role = "role::${subrole_lvl1}::${subrole_lvl2}::${subrole_lvl3}::${subrole_lvl4}"
} elsif $subrole_lvl3 != '' {
  $role = "role::${subrole_lvl1}::${subrole_lvl2}::${subrole_lvl3}"
} elsif $subrole_lvl2 != '' {
  $role = "role::${subrole_lvl1}::${subrole_lvl2}"
} elsif $subrole_lvl1 != '' {
  $role = "role::${subrole_lvl1}"
} else {
  $role = 'role'
}

hiera_include('profiles')

node default {}
